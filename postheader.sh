#!/usr/bin/env bash
pandoc --template pandoc-template.html -H ./Documents/About_Me.wiki.html.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/About_Me.html.html -s ./Documents/About_Me.wiki.html.wiki
pandoc --template pandoc-template.html -H ./Documents/Blogroll.wiki.html.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/Blogroll.html.html -s ./Documents/Blogroll.wiki.html.wiki
pandoc --template pandoc-template.html -H ./Documents/index.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/index.html -s ./Documents/index.wiki
pandoc --template pandoc-template.html -H ./Documents/My_Bookmarks.wiki.html.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/My_Bookmarks.html.html -s ./Documents/My_Bookmarks.wiki.html.wiki
pandoc --template pandoc-template.html -H ./Documents/Read_more_about_my_Windows_apps.wiki.html.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/Read_more_about_my_Windows_apps.html.html -s ./Documents/Read_more_about_my_Windows_apps.wiki.html.wiki
pandoc --template pandoc-template.html -H ./Documents/VIM.wiki.html.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/VIM.html.html -s ./Documents/VIM.wiki.html.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2020-12-11.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2020-12-11.html -s ./Documents/journal/2020-12-11.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2020-12-15.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2020-12-15.html -s ./Documents/journal/2020-12-15.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2020-12-21.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2020-12-21.html -s ./Documents/journal/2020-12-21.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-01-10.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-01-10.html -s ./Documents/journal/2021-01-10.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-01-14.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-01-14.html -s ./Documents/journal/2021-01-14.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-01-25.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-01-25.html -s ./Documents/journal/2021-01-25.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-01-29.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-01-29.html -s ./Documents/journal/2021-01-29.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-02-04.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-02-04.html -s ./Documents/journal/2021-02-04.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-02-10.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-02-10.html -s ./Documents/journal/2021-02-10.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-02-18.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-02-18.html -s ./Documents/journal/2021-02-18.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-02-21.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-02-21.html -s ./Documents/journal/2021-02-21.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-03-04.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-03-04.html -s ./Documents/journal/2021-03-04.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-03-14.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-03-14.html -s ./Documents/journal/2021-03-14.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-03-21.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-03-21.html -s ./Documents/journal/2021-03-21.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-04-21.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-04-21.html -s ./Documents/journal/2021-04-21.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-04-29.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-04-29.html -s ./Documents/journal/2021-04-29.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-05-10.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-05-10.html -s ./Documents/journal/2021-05-10.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-05-27.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-05-27.html -s ./Documents/journal/2021-05-27.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-06-16.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-06-16.html -s ./Documents/journal/2021-06-16.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-06-28.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-06-28.html -s ./Documents/journal/2021-06-28.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-07-14.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-07-14.html -s ./Documents/journal/2021-07-14.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-07-27.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-07-27.html -s ./Documents/journal/2021-07-27.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-08-12.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-08-12.html -s ./Documents/journal/2021-08-12.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-08-21.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-08-21.html -s ./Documents/journal/2021-08-21.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-08-26.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-08-26.html -s ./Documents/journal/2021-08-26.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-09-06.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-09-06.html -s ./Documents/journal/2021-09-06.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-09-15.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-09-15.html -s ./Documents/journal/2021-09-15.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-09-18.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-09-18.html -s ./Documents/journal/2021-09-18.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-09-25.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-09-25.html -s ./Documents/journal/2021-09-25.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-09-28.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-09-28.html -s ./Documents/journal/2021-09-28.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-10-03.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-10-03.html -s ./Documents/journal/2021-10-03.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-10-11.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-10-11.html -s ./Documents/journal/2021-10-11.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-10-24.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-10-24.html -s ./Documents/journal/2021-10-24.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-11-03.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-11-03.html -s ./Documents/journal/2021-11-03.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-11-20.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-11-20.html -s ./Documents/journal/2021-11-20.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-11-24.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-11-24.html -s ./Documents/journal/2021-11-24.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-12-03.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-12-03.html -s ./Documents/journal/2021-12-03.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-12-07.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-12-07.html -s ./Documents/journal/2021-12-07.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-12-14.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-12-14.html -s ./Documents/journal/2021-12-14.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2021-12-25.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2021-12-25.html -s ./Documents/journal/2021-12-25.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-01-04.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-01-04.html -s ./Documents/journal/2022-01-04.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-01-10.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-01-10.html -s ./Documents/journal/2022-01-10.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-01-19.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-01-19.html -s ./Documents/journal/2022-01-19.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-01-23.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-01-23.html -s ./Documents/journal/2022-01-23.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-02-04.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-02-04.html -s ./Documents/journal/2022-02-04.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-02-10.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-02-10.html -s ./Documents/journal/2022-02-10.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-02-23.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-02-23.html -s ./Documents/journal/2022-02-23.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-03-03.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-03-03.html -s ./Documents/journal/2022-03-03.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-03-08.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-03-08.html -s ./Documents/journal/2022-03-08.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-03-13.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-03-13.html -s ./Documents/journal/2022-03-13.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-03-23.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-03-23.html -s ./Documents/journal/2022-03-23.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-03-27.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-03-27.html -s ./Documents/journal/2022-03-27.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-04-03.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-04-03.html -s ./Documents/journal/2022-04-03.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-04-11.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-04-11.html -s ./Documents/journal/2022-04-11.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-04-14.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-04-14.html -s ./Documents/journal/2022-04-14.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-04-22.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-04-22.html -s ./Documents/journal/2022-04-22.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-04-27.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-04-27.html -s ./Documents/journal/2022-04-27.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-05-03.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-05-03.html -s ./Documents/journal/2022-05-03.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-05-12.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-05-12.html -s ./Documents/journal/2022-05-12.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-05-16.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-05-16.html -s ./Documents/journal/2022-05-16.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-05-22.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-05-22.html -s ./Documents/journal/2022-05-22.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-05-28.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-05-28.html -s ./Documents/journal/2022-05-28.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-06-09.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-06-09.html -s ./Documents/journal/2022-06-09.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-06-13.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-06-13.html -s ./Documents/journal/2022-06-13.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-06-17.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-06-17.html -s ./Documents/journal/2022-06-17.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-06-22.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-06-22.html -s ./Documents/journal/2022-06-22.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-06-26.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-06-26.html -s ./Documents/journal/2022-06-26.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-07-01.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-07-01.html -s ./Documents/journal/2022-07-01.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-07-03.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-07-03.html -s ./Documents/journal/2022-07-03.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-07-11.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-07-11.html -s ./Documents/journal/2022-07-11.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-07-17.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-07-17.html -s ./Documents/journal/2022-07-17.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-07-21.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-07-21.html -s ./Documents/journal/2022-07-21.wiki
pandoc --template pandoc-template.html -H ./Documents/journal/2022-07-26.wiki.hdr --resource-path=$(PWD) -s -f mediawiki  -o ./content/2022-07-26.html -s ./Documents/journal/2022-07-26.wiki
./node_modules/.bin/bs export --web Documents/presentations/'Remarkpy Example.md' -o public/presentations/Remarkpy_Example/
