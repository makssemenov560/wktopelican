#!/usr/bin/env python

from bs4 import BeautifulSoup as bs
import urllib3
import sys

if __name__ == "__main__":
    # upstream repo
    url = "https://gitlab.com/Sevepy/wktopelican/-/raw/master/public/My_Bookmarks.wiki.html"
    http = urllib3.PoolManager()
    try:
        response = http.request("GET", url)
    except:
        # no internet connection
        sys.exit()
    # ----------------------------------|
    #                                   v Default parser
    html_bookmarks = bs(response.data, "lxml")

    # filter out part of interest
    inner_article = html_bookmarks.find("div", class_="article-content")

    # get the urls from the soup :->
    list_of_bookmarks = inner_article.find_all("a", recurive=False)

    # pelican wants a title
    header = """
<!DOCTYPE NETSCAPE-Bookmark>
<head>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=UTF-8">
<TITLE>Bookmarks</TITLE>
</head>
<body>
"""
    with open("./public/mybookmarks.html", "w") as bookmarks:
        bookmarks.write(header)
        # list of
        bookmarks.write(
            "\n".join(
                ["<DT>" + anchor + "</DT>" for anchor in map(str, list_of_bookmarks)]
            )
        )
        bookmarks.write("</body>")
