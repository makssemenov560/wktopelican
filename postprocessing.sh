#!/usr/bin/env bash

for f in `find ./public -iname '*.wiki.html.wiki.html' -type f -print`;do  mv "$f" ${f%.wiki.html.wiki.html}.wiki.html; done

#fixes tipuesearch_content.js file paths for pages html output
sed -i 's/wiki.html.wiki.html/wiki.html/g' public/tipuesearch_content.js
#find ./public -name "*.wiki.html.wiki.html" -exec bash -c 'mv "$1" "${1%.wiki.html.wiki.html}".wiki.html' - '{}' +
