#!/usr/bin/env bash
rm ./Documents/*.hdr
rm ./Documents/journal/*.hdr
rm ./content/*.html
rm ./content/*.ipynb
rm ./content/*.nbdata
#rm -rf ./public/*
rm ./tags/*
