import subprocess
import json
from os import walk, path
import datetime
import pathlib
import shutil
import pickle
import pypandoc

# pandoc command parametric string
scriptcontent = "pandoc --template pandoc-template.html -H {hdr} --resource-path=$(PWD) -s -f mediawiki  -o ./content/{output} -s {inp}"

scf = open("postheader.sh", "w")
scf.write("#!/usr/bin/env bash\r\n")
for (root, dirnames, filenames) in walk("./Documents"):
    for file in filenames:
        if file == "index.wiki":
            slug = "indexwiki"
        else:
            slug = str(file).replace(" ", "_")
        if root.find("journal") != -1:
            category = "Journal"
        else:
            category = "Pages"

        fname = pathlib.Path(path.join(root, file))

        # read tags from serialised list of keywords
        try:
            with open("./tags/" + file + ".pickle", "rb") as pck_file:
                list_of_keywords = pickle.load(pck_file)
                tags = ",".join([item.capitalize() for item in list_of_keywords])
        except OSError:
            tags = ""

        ## use cdate  from title if journal
        if category == "Journal":
            modified = str(
                datetime.datetime.fromisoformat((fname.stem).replace(".wiki", ""))
            )
        else:
            modified = str(datetime.datetime.fromtimestamp(fname.stat().st_mtime))
        # change time
        date = str(datetime.datetime.fromtimestamp(fname.stat().st_ctime))
        authors = "@seve_py"

        if category == "Pages":
            meta = """
    <meta name="tags" content="{tags}" />
    <meta name="date" content="{date}" />
    <meta name="modified" content="{modified}" />
    <meta name="category" content="{category}" />
    <meta name="authors" content="{authors}" />
    <meta name="slug" content="{slug}" />
    <meta name="status" content="{status}" />
            """
        else:

            meta = """
    <meta name="tags" content="{tags}" />
    <meta name="date" content="{modified}" />
    <meta name="modified" content="{date}" />
    <meta name="category" content="{category}" />
    <meta name="authors" content="{authors}" />
    <meta name="slug" content="{slug}" />
    <meta name="status" content="{status}" />
            """
        ## metafile for Jupyter Notebooks
        ## needs a empty line at the end of the metafile
        ## predefined templates basic, full tpl  celltags.tpl  full.tpl  mathjax.tpl  slides_reveal.tpl

        ## Remark presentations
        if file.endswith(".md"):
            scriptcontent = (
                "./node_modules/.bin/bs export --web Documents/presentations/'{inp}' -o public/presentations/"
                + file.replace(".md", "").replace(" ", "_")
                + "/"
            )
            scf.write(scriptcontent.format(inp=file))
            scf.write("\r\n")
        elif file.endswith(".ipynb") and file.find("-checkpoint") == -1:
            # check if filename has (DRAFT)
            if file.find("DRAFT") > -1:
                status = "draft"
            else:
                status = "published"
            meta = """Title: {title}
Date: {date}
Category: IPYNB
Slug: {slug}
Tags: {tags}
Author: {authors}
Status: {status}
Summary:
template_file:basic
"""
            with open(
                "./content/" + file.replace(".ipynb", "") + ".nbdata", "w"
            ) as ipynbmeta:
                ipynbmeta.write(
                    meta.format(
                        title=file.replace(".ipynb", ""),
                        date=modified,
                        slug=file.replace(" ", "_"),
                        tags="jupyter notebook",
                        authors=authors,
                        status=status,
                    )
                )
            shutil.copy(path.join(root, file), "./content/" + file)

        ## journal or pages
        elif (
            not file.endswith(".hdr")
            and file.find("-checkpoint") == -1
            and not file.endswith(".md")
        ):
            # check if filename has (DRAFT)
            if file.find("DRAFT") > -1:
                status = "draft"
            else:
                status = "published"
            with open(path.join(root, file) + ".hdr", "w") as fp:
                fp.write(
                    meta.format(
                        tags=tags,
                        date=date,
                        modified=modified,
                        category=category,
                        authors=authors,
                        slug=slug,
                        status=status,
                    )
                )
            basename = file.replace(".wiki", "")
            winroot = root.replace("\\", "/") + "/"
            scf.write(
                scriptcontent.format(
                    hdr=winroot + file + ".hdr",
                    output=basename + ".html",
                    inp=winroot + file,
                )
            )
            scf.write("\r\n")

scf.close()
