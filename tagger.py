#!/usr/bin/env python
import sys
import os
import pathlib
import pickle

# https://pypi.org/project/wikitextparser/ super-useful
# pip did not work, cloned from https://github.com/5j9/wikitextparser
# python setup.py build
# python setup.py install
from wikitextparser import remove_markup, parse
import re
from sklearn.feature_extraction._stop_words import ENGLISH_STOP_WORDS
import yake

# https://github.com/LIAAD/yake

if __name__ == "__main__":

    language = "en"
    max_ngram_size = 2
    deduplication_thresold = 0.9
    deduplication_algo = "seqm"
    windowSize = 1
    numOfKeywords = 6
    numOfKeywordsDumped = 4

    ## excluded_kw
    excluded_kw = ["remote","file","files","test","physical", "na", "set","find","usr","date","path","ctrl","start","start-start","problem","kbd"]
    excluded_kw.append(["working","living","drawing","writing","quitting","modifying","compiling","natively"])
    excluded_kw.append(ENGLISH_STOP_WORDS)
    custom_kw_extractor = yake.KeywordExtractor(
        lan=language,
        n=max_ngram_size,
        dedupLim=deduplication_thresold,
        dedupFunc=deduplication_algo,
        windowsSize=windowSize,
        top=numOfKeywords,
        features=None,
    )
    for (root, dirnames, filenames) in os.walk("./Documents"):
        for file in filenames:
            if file.endswith(".wiki"):
                mediawiki_file = os.path.join(root, file)
                with open(mediawiki_file, encoding="utf8") as wiki:
                    text = re.sub(r"<pre>(.*?)</pre>", "", wiki.read(), flags=re.DOTALL)
                    text = re.sub(r"==(.*?)==", "\1\.", text, flags=re.DOTALL)
                    text = parse(text).plain_text()
                keywords = custom_kw_extractor.extract_keywords(text)

                # print("\n\n{}".format(keywords))
                pickle_file_handler = file + ".pickle"
                # TODO make sure tags dir exists
                with open("./tags/" + file + ".pickle", "wb") as pickle_hnd:
                    unfiltered_kw = [kw[0] for kw in keywords]
                    filtered = []
                    for ukw in unfiltered_kw:
                        if ukw.find(".") > -1:
                            pass
                        elif ukw.lower() in excluded_kw:
                            pass
                        elif ukw.find("Python")>-1:
                            filtered.append("Python")
                        elif ukw.find("Windows")>-1:
                            filtered.append("Windows")
                        elif ukw.find("Scientific")>-1:
                            filtered.append("GSL")
                        else:
                            filtered.append(ukw)
                    if len(filtered) > 4:
                        pickle.dump(filtered[0:numOfKeywordsDumped], pickle_hnd)
                    else:
                        pickle.dump(filtered, pickle_hnd)
