#!/usr/bin/env bash
#git ls-files -z | xargs -0 -n1 -I{} -- git log -1 --format="%ai {}" {} | perl -ne 'chomp;next if(/'"'"'/);($d,$f)=(/(^\d\d\d\d-\d\d-\d\d \d\d:\d\d:\d\d(?: \+\d\d\d\d|)) (.*)/);print "d=$d f=$f\n"; `touch -d "$d" '"'"'$f'"'"'`;'

IFS="
"
for FILE in $(git ls-files)


do
    if [[ $FILE =~ "Documents"  ]]; then 
        TIME=$(git log  --reverse --pretty=format:%ai --date=iso -- "$FILE"|head -1)
        TIME=$(date -d "$TIME"  +%Y-%m-%d\ %H:%M:%S)
        printf "$TIME  $FILE \n"
        ## change modified time, see with stat
        touch -d "$TIME" "$FILE"
    fi
done

