
== TIG Browser - VIM Plugin ==

The new major release of Vim (version 9.0) supports a new script language ([https://vimhelp.org/vim9.txt.html VIM9SCRIPT]) intended to drastically
improve performance. This [https://gitlab.com/Sevepy/vim-tig single file
plugin] opens [https://jonas.github.io/tig/ TIG], a Git
repository browser, inside a ''popup window''.

TIG has to be [https://jonas.github.io/tig/INSTALL.html installed] separately,
on Windows, [https://gitforwindows.org Git for Windows SCM] contains both binaries ''git.exe'' and ''tig.exe''.


=== VIM9SCRIPT Plugin ===

Vim-Tig has been entirely written in Vim9Script and therefore it works only with VIM version 9.0. 
The [https://gitlab.com/Sevepy/vim-tig/-/raw/a92de1d1b5e0a2872ee24796cedf4d88f7e51eda/plugin/tig.vim main script file] in the ''plugin'' directory has ''vim9script'' as its first command which indicates that isn't a legacy VIML script. Functions are defined with the ''def'' command and are declared with argument types and return type.


YouTubeVideo("aFwnhLUSXfI")

As shown in the video above, a central floating window (''popup'') will list
all the ''commits'' in the repository. Floating windows have been recently
included in VIM ( version 8.2 ), in this plugin the buffer displayed inside the
floating window is the terminal where tig is running.


=== MINGW64/MSYS2 === 

Tig can be installed with:
    pacman -S tig

You might encounter a problem with the Windows shell when the PATH environmental variable doesn't include the folder where TIG is installed. Tig needs also git.exe to be included in the PATH. 
The bat file below simulates the same configuration provided by ''Git for windows''.
    
    <pre>
    @echo off
    cd C:\Users\seve\workplace\msys2\usr\bin
    set PATH=%PATH%;C:\Users\Seve\workplace\msys2\usr\bin
    set todir=%1
    set repo=%2

    IF %repo%.==. GOTO ShowVer

    tig.exe %todir% %repo%
    GOTO EndBat

    :ShowVer
    tig.exe -v
    GOTO EndBat

    :EndBat
    </pre>
