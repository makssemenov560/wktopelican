=== Remarkpy theme - AsciiMath - PyScript ===

[http://asciimath.org AsciiMath] is an easy-to-write markup language for mathematics. It's easier than Latex and it's supported by MathJax display engine. Unfortunately, markdown to HTML renders often uses asciimath delimiters ''`...`'' for code blocks.

[https://www.buymeacoffee.com/seve/remarkpy-windows-markdown-presentation-tool Remarkpy] has a dedicated theme that replaces the default delimiters with a '''!!!'''.   

Image("asciimath.PNG")


The same theme includes also the [https://pyscript.net PyScript] library which allows running and displaying the output of python scripts directly inside a presentation, as shown in the figure below.


Image("pyscript.PNG")


