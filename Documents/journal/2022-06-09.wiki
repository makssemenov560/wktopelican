
== Note-taking desktop application and GVIM ==

GVIM the VIM text editor GUI version for Windows has two different executables, ''vim.exe'' which opens inside a terminal in this case Windows Terminal (wt.exe), and ''gvim.exe'' which opens VIM as a standalone desktop application.

As shown in the video below, it's possible to set either gvim.exe or vim.exe as the text editor for my note-taking desktop application. The full path is necessary when they are not recognized as internal or external commands i.e. the folder is not included in the environmental variable ''PATH''.


=== Open URIs ===

Several hyperlinks and URIs are presented when the letter ''l'' is pressed (second part of the video) after displaying the note inside the terminal. [https://www.buymeacoffee.com/seve/download-install-windows-desktop-applications Wintermnote] (ver. 0.1.22) recognizes PDF files and several figure formats, and it will try to open them with the default program associated with their file types.

It also recognizes [https://tessarinseve.pythonanywhere.com/nws/2022-01-23.wiki.html cbthunderlinks] and opens them with Mozilla Thunderbird.


YouTubeVideo("6uNm0BMsxks")
