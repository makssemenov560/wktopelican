
== Search jQuery plugin for static websites ==

As my website continues to grow, I've decided that it's the right time to add a means for searching a particular term inside my blog posts. Apparently, it seemed something relatively easy to do, but it turned out to be more complex than I thought. 

'''''Tipue search''''' plugin for Pelican exists, it can provide the tool for serialising a website content inside an index file. but I was not able to install it with ''pip'', Python package manager. I had to clone the entire git tree from the following [https://github.com/pelican-plugins/tipue-search repository] and then copied the directory ''tipue_search'' in the same path alongside other plugins.

I included this plugin to the plugins' list in my ''pelicanconf.py'' file:

    PLUGINS = [
        ...
        'tipue_search',
        ]

Now, when I compile the website I automatically get the index file ''tipuesearch_content.js'' in the root folder, you can see it [https://tinyurl.com/ydk72xyn here]. 

That was the easy part.

Since ''Tipue search'' is a generic search engine, it has to be adapted to a Pelican static website with a Bootstrap-based theme. I created a ''search.html'' template file as follows:
    <pre>
    {% extends "base.html" %}

    {% block title %}
      Search {{ super() }}
    {% endblock title %}

    {% block content %}

    <section id="content" class="body">
    <div class="row">
        <div class="col-md-4 mt-5">
            <input class="form-control rounded-pill py-2 border-right-0 border" type="search" placeholder="Search" id="tipue_search_input">
        </div>
      </div>
    <div id="tipue_search_content"></div>
    </section>

    <script>


    window.addEventListener('DOMContentLoaded', function() { 
    $(document).ready(function() {
         $('#tipue_search_input').tipuesearch({
             'show': 10,
             'mode': 'json',
             'contentLocation': '{{ SITEURL }}/tipuesearch_content.json'
         });
    });

    });
    </script>


    {% endblock %}
    </pre>

Note that the ''ready'' function will wait until the jQuery library is fully loaded. Finally, I have included in the file header the following style and javascript extensions.

    <pre>
    <link href="{{ SITEURL }}/theme/css/tipuesearch.css" rel="stylesheet">
    <script src="{{ SITEURL }}/tipuesearch_content.js"></script>

    <script src="{{ SITEURL }}/theme/js/tipuesearch_set.js"></script>
    <script src="{{ SITEURL }}/theme/js/tipuesearch.js"></script>
    </pre>

They can be extracted from Pelican's Plumage theme or from my upstream repository.

It's true, I could have embedded the search box in the navigation bar to avoid an extra click. But I felt this was unnecessary and it would have added extra complexity compared to a dedicated search page.

=== Final comment ===

This approach seems to work only when the webpage is browsed from a web server. If you don't have a web server at hand you can use  Pelican's own web server:

    $ make serve

from the command line and open localhost:8000 in your default browser.


=== Postscript ===

==== 2021-11-25 ====

Github tipue-search repository is no longer maintained, the ''README'' for the recommended new Pelican search plugin can be found [https://github.com/pelican-plugins/search here].
