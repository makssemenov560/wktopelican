== [https://www.buymeacoffee.com/seve/gnu-diction-windows-app-eu-english-style WinDiction] v.0.0.6 ==

WinDiction, a Windows app that compares documents against a set of predefined style rules, has improved support for MediaWiki and Mardown formatted files. It automatically removes the markup elements from the text to get more readable suggestions. It has also been extended to include DOCX (Word) and ODT (LibreOffice Writer) file types.

Furthermore, different language styles (German, Dutch, English-American) can now be selected from an editable configuration file.

To support this project and download the app, [https://www.buymeacoffee.com/seve/gnu-diction-windows-app-eu-english-style buy me a coffee].
