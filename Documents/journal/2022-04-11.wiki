
== Command Line Word Processor with MikTeX and Pandoc ==

This note expands [https://tessarinseve.pythonanywhere.com/nws/2022-03-08.wiki.html on a previous one] where I have described how ''Pandoc'' can create LibreOffice documents from markdown files.

This time, I will try to produce a PDF version of a more complex document, such as a book, from a set of markdown files. [https://pandoc.org/MANUAL.html#pandocs-markdown Pandoc's markdown syntax] can be used to include important document elements not available in plain markdown.

=== MikTeX Portable ===

Pandoc requires a ''PDF engine'' that can be obtained from the MikTex typesetting system. Installed as [https://miktex.org/howto/portable-edition portable] on a USB stick will save considerable disk space (~ 1 GB).

=== Set Tex/LaTex Document Class ===

When Pandoc converts markdown to pdf it passes through an intermediate ''Tex'' file. A Tex/Latex file has a section called ''Preamble'' made of commands located before the code block:
    <pre>
    \begin{document}
    ...
    \end{document} 
    </pre>

To add the built-in ''book'' document class inside the preamble, I can set the following Pandoc's command line option: 

    -V documentclass=book

=== Pandoc template ===

The default Pandoc template for md to tex conversion can be generated from the command line with: 

    $ pandoc.exe -D latex > book.tex

and eventually customised, if necessary.

=== Pandoc YAML header ===

Several document elements in the preamble, such as the book's cover page can be defined inside a YAML file.  
    <pre>
    ---
    title: Word Processor with MikTeX and Pandoc 
    author: seve_py
    papersize: A4
    ---
    </pre>

=== Bash Script ===

Finally, I can run the bash script below to generate the book in PDF format.

    <pre>
    #!/usr/bin/env bash

    # PDF latex engine from miktex-potable
    PDFLATEX="D:\\miktex-portable\\texmfs\\install\\miktex\\bin\\x64\\pdflatex.exe"

    # Directory containing Markdown files
    source="Project/book"

    pandoc \
        --template=./Project/book/book.tex \
        --from markdown \
        --toc -N \
        -V documentclass=book \
        --listings \
        --lua-filter=C:\\Users\\seve\\.pandoc\\filters\\pagebreak.lua \
        --pdf-engine=$PDFLATEX \
        -o Publish/book.pdf \
        $source/book.yml \
        $source/chapter1.md

    </pre>

=== Macros Preprocessor ===

GPP macro preprocessor has been installed from [https://www.buymeacoffee.com/seve/gnu-diction-windows-app-eu-english-style Windiction]. It allows to preprocess a markdown file with GPP and pass the output to Pandoc.

    gpp -T --include macros.gpp


