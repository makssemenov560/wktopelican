
== VIM snippets ==

Code snippets are reusable code blocks that can save considerable typing.  ''COC'' includes a [https://tessarinseve.pythonanywhere.com/nws/2022-01-04.wiki.html snippets engine] that supports ''snipMate'' format and provides better compatibility for (g)Vim.  The snipMate plugin isn't necessary after you have installed the COC extension, and the user's hotkeys can be defined in the .vimrc file.     
    <pre>
    "" coc snippets config as per my .vimrc
    "" next placeholder
    let g:coc_snippet_next = '<M-n>'

    "" jump to previous placeholder
    let g:coc_snippet_prev = '<M-b>'

    imap <M-n> <Plug>(coc-snippets-expand-jump)
    </pre>

This set <kbd>Alt</kbd>-<kbd>n</kbd> as the snippet's expansion key when in insert mode.

The original snipMate ''extends'' directive won't work and it has to be declared in the configuration file as shown below:

    <pre>

    // coc config file for gvim 
    {
    "coc.source.bibtex.filetypes":["tex","latex", "pandoc", "markdown", "notes"],
    "list.source.bibtex.files":["C:\\Users\\Seve\\.pubs\\pubs.bib"],
    "snippets.ultisnips.enable": false, // required if python not installed
    "snippets.loadFromExtensions": false,
    "snippets.extends":{
    "notes":["ascii"],
    "markdown":["html"] //tell coc to also read html snippets
    },
    "coc.source.emoji.filetypes": [
        "notes"
   ]
    }

    </pre>

Snippets for markdown files will also include those intended for HTML files.

SnipMate Filename() function is also not supported, but it can be replaced with:
    snippet filename "filename without extension" 
        `expand('%:r')`

=== VLANG scripts ===

I am not aware of an existing set of snippets for VLANG that can be downloaded. However, it isn't difficult to code one from scratch. First, it's necessary to set the filetype ''vlang'' for new files with the extension .v and .vsh.
    <pre>
    "" vlang .vimrc
    augroup vlang
        au BufRead,BufNewFile *.v,*.vsh set filetype=vlang
    augroup END
    </pre>
Then I can create a vlang.snippets file in the directory ''.vim/snippets/'' 
    <pre>
    snippet script "VLANG script" 
        #!/Users/Seve/workplace/v/v run
        ${0://code }
    snippet prf "println function output"
        println(${1}("${2}") ?)
    ...
    </pre>

The video below shows a simple VLANG script built from the previous snippets.

YouTubeVideo("wdObk6ml7hk")


