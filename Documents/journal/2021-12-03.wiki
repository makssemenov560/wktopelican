
== Software development with CSVTODO ==

CSVTODO has not been created with a specific domain in mind, it's a Todo desktop application that can be used for all sorts of problems. However, as I would like to demonstrate here, it can be especially valuable for software developers.

CSVTODO has three types of ''annotations'' associated with a particular task:

N) A standard text file normally saved into the ''note'' folder,

L) A hyperlink to a webpage or a web resource,

R) A local GIT repository.

N, L, and R will be displayed in the last column of the list. 

The figure below shows the ''annotation'' submenu, visible after right clicking on top of a selected task. Followed by a click on the option ''Local Repository'', a standard dialog allows the user to select a GIT local folder (CSVTODO checks if the folder has .git directory). 

Image("csvtodo_add_repository.png")

With a double click on the same row, Windows file explorer will open on the selected GIT repository.

=== GIT tags ===

[https://git-scm.com/book/en/v2/Git-Basics-Tagging Assign tags] is an important feature of modern version control systems, they mark specific points in the repository's history. GIT tags can be attached to a specific ''commit'', often associated with a version release or a notable refactoring.

CSVTODO programmatically creates a ''post-commit hook'' ( you can read [https://tessarinseve.pythonanywhere.com/nws/2021-08-26.wiki.html here] about GIT-hooks ) when the status of the task changes to ''Completed''.

This will define ''an annotated tag'' where the tag's label is the task's context and the message is the task's definition.
Here's an example of self-generated post-commit hook:
    
    #!/usr/bin/env bash
    git tag -a "v0.1.8" -m "CSVTODO add support for git tags"

From the command line, it's possible to display the tags defined for the local repository:
    <pre>
    Seve (master)
     ~/workspace/csvtodo>git tag -n
    v0.1.8          CSVTODO support for git tag
    </pre>

Then, you can use the tag as a reference to a commit:
   
    <pre>
    $ git show v0.1.8 
    tag v0.1.8   <---- Tag
    Tagger: U-DESKTOP-L87ENT7\Seve <....@gmail.com>
    Date:   Fri Dec 3 18:31:38 2021 +0000

    CSVTODO support for git tag <---- Message

    commit ef5e5a76d83c2c1f0f187e4b7cf02d1d6561177f (HEAD -> master, tag: v0.1.8, origin/master, origin/HEAD)
    Author: U-DESKTOP-L87ENT7\Seve <....@gmail.com>
    Date:   Fri Dec 3 18:31:38 2021 +0000

        Support git repos and tags
    </pre>

Tags are local and have to be explicitly pushed to a remote repository with:
    $ git push --tags

Image("gitlab_tags.png")

